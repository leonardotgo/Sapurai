﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class AITriggerCollider : MonoBehaviour {

    public NPC targetAI;
    public LayerMask collidableObjects;

    public UnityEvent OnTrigger;
    
    public void OnTriggerEnter(Collider other)
    {        
        if (LayerUtility.IsInLayerMask(other.gameObject.layer, collidableObjects))
        {
            OnTrigger.Invoke();
            if(targetAI != null)
                targetAI.Notify<AITriggerCollider>(this, other.gameObject, true);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (LayerUtility.IsInLayerMask(other.gameObject.layer, collidableObjects))
        {
            OnTrigger.Invoke();
            if (targetAI != null)
                targetAI.Notify<AITriggerCollider>(this, other.gameObject, false);
        }
    }
}
