﻿using UnityEngine;
using System.Collections;

public class MovementAnimator : CharacterAnimator
{
    public void SetAnimatorSpeed(float speed)
    {
        animator.speed = speed;
    }

    public void SetSpeed(float newSpeed)
    {
        animator.SetFloat("Speed", newSpeed);
    }

    public void SetDirection(Vector2 dir)
    {
        animator.SetFloat("dir_x", dir.x);
        animator.SetFloat("dir_z", dir.y);
    }

    public void SetTrigger(string trigger)
    {
        animator.SetTrigger(trigger);
    }
}
