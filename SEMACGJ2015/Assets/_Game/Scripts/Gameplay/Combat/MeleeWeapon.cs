﻿using UnityEngine;
using System.Collections;

public class MeleeWeapon : Weapon
{
    public WeaponHitbox hitbox;
    public GameObject visualEffect;
    public AudioClip swingSound;
    public AudioClip hitSound;
    public Animator effectAnimator;

    private void Start()
    {
    }

    public override void OnUse()
    {
        base.OnUse();
    }

    public override void OnHit(Character target)
    {
        base.OnHit(target);

        target.ChangeHealth(-damage);
        if (knockbackStrength > 0f)
        {
            Vector3 knockbackDir = (target.transform.position - wielder.transform.position).normalized;
            CombatEffect.Knoback knockback = new CombatEffect.Knoback(knockbackDir, knockbackStrength);
            knockback.Apply(target);
        }

        if (hitSound != null)
            wielder.PlaySound(hitSound);
        
        Camera.main.GetComponent<TopDownCamera>().ShakeCamera(damage / 5f);
    }

    public override void AnimationEvent(string message)
    {
        base.AnimationEvent(message);

        if (message == "StartHitbox")
            hitbox.gameObject.SetActive(true);
        else if (message == "StopHitbox")
            hitbox.gameObject.SetActive(false);
        else if (message == "SwingSound")
        {
            if (swingSound != null)
                wielder.PlaySound(swingSound);
        }
        else if (message == "WeaponEffect")
            if (effectAnimator != null)
                effectAnimator.SetTrigger("Start");
    }
}