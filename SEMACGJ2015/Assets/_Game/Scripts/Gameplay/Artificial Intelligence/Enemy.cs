﻿using UnityEngine;
using System.Collections;

public class Enemy : NPC {

    public Color color = Color.white;

    protected override void Start()
    {
        base.Start();
    }

    public override void Notify<T>(MonoBehaviour sender, params object[] args)
    {
        base.Notify<T>(sender, args);            
    }

    public override void OnDeath()
    {
        base.OnDeath();
        GameController.OnKillEnemy();
    }


    protected override void Awake()
    {
        base.Awake();
    }

    
}
