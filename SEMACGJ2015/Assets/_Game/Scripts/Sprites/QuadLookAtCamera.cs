﻿using UnityEngine;
using System.Collections;

public class QuadLookAtCamera : MonoBehaviour {

    private Transform cam;

    void Start()
    {
        cam = Camera.main.transform;
    }

	// Update is called once per frame
	void Update () {
        transform.LookAt(cam.transform, Vector3.up);
        transform.forward = -transform.forward;
	}
}
