﻿using UnityEngine;
using System.Collections;

public class LevelGenerator : MonoBehaviour {

    private int randomWallSlot
    {
        get {
            int slot;
            do
            {
                slot = Random.Range(0, wallSlots.Length - 1);
            } while (wallSlotsObjects[slot] == true);
            return slot;
        }
    }

    private int lastFreeWallSlot
    {
        get
        {
            int slot = -1;
            for (int i = 0; i < wallSlots.Length; i++)
                if (!wallSlotsObjects[i])
                    slot = i;
            return slot;
        }
    }


    public Enemy[] enemyPrefabs;

    public Transform[] wallSlots;
    public GameObject[] wallProps;
    private GameObject[] wallSlotsObjects;

    public Transform[] enemySlots;

    public GameObject[] walls;
    public GameObject entrance;
    public GameObject exit;

    public int maxEnemies = 15;
    public float propAtWallChance = 0.2f;

    [HideInInspector]
    public int lastExitSlot = -1;

    public void GenerateMap(int level)
    {
        ClearPreviousProps();
        ClearPreviousEnemies();
        FreeWallSlots();
        GenerateEntrances();
        GenerateWalls();

        GenerateEnemies(level);
    }

    public void GenerateEnemies(int level)
    {
        int numberOfEnemies = (level <= maxEnemies) ? level : maxEnemies;
        if (enemyPrefabs.Length > 0)
        {
            GameController.instance.enemiesToKill = numberOfEnemies;
            for (int i = 0; i < numberOfEnemies; i++)
            {
                Enemy enemy = GameObject.Instantiate<Enemy>(enemyPrefabs[Random.Range(0, enemyPrefabs.Length)]);
                SetToTransform(enemy.gameObject, enemySlots[Random.Range(0, enemySlots.Length - 1)]);
            }
        }
    }

    public void FreeWallSlots()
    {
        wallSlotsObjects = new GameObject[wallSlots.Length];
        for (int i = 0; i < wallSlots.Length; i++)
            wallSlotsObjects[i] = null;

    }

    public void ClearPreviousProps()
    {
        GameObject[] objects = FindObjectsOfType<GameObject>();
        for (int i = 0; i < objects.Length; i++)
            if (objects[i].tag == "Props")
                GameObject.Destroy(objects[i]);
    }

    public void ClearPreviousEnemies()
    {
        Enemy[] characters = FindObjectsOfType<Enemy>();
        for (int i = 0; i < characters.Length; i++)
                GameObject.Destroy(characters[i].gameObject);
    }

    public void GenerateEntrances()
    {
        int entranceSlot;
        if (lastExitSlot == -1)
            entranceSlot = randomWallSlot;
        else
            entranceSlot = wallSlots.Length - lastExitSlot - 1;

        SetToTransform(entrance, wallSlots[entranceSlot]);
        wallSlotsObjects[entranceSlot] = entrance;

        int exitSlot = randomWallSlot;
        SetToTransform(exit, wallSlots[exitSlot]);
        wallSlotsObjects[exitSlot] = exit;
        lastExitSlot = exitSlot;

        exit.GetComponent<Collider>().enabled = true;
    }

    public void GenerateWalls()
    {
        for (int i = 0; i < walls.Length; i++)
        {
            int wallSlot = lastFreeWallSlot;
            SetToTransform(walls[i], wallSlots[wallSlot]);
            wallSlotsObjects[wallSlot] = walls[i];

            if (wallProps.Length > 0)
            {
                bool spawnProp = (Random.Range(0f, 1f) < propAtWallChance);
                if (spawnProp)
                {
                    GameObject prop = GameObject.Instantiate(wallProps[Random.Range(0, wallProps.Length)]);
                    SetToTransform(prop, wallSlots[wallSlot]);
                }
            }
        }
    }

    public void SetToTransform(GameObject obj, Transform transform)
    {
        obj.transform.position = transform.position;
        obj.transform.rotation = transform.rotation;
    }
}
