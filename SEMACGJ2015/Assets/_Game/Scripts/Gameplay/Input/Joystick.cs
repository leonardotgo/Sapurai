﻿using UnityEngine;
using System.Collections;
using System;

public class JoystickButton
{
    public string name;
    public Type commandType;

    public JoystickButton(string _name)
    {
        this.name = _name;
    }

    public JoystickButton(string _name, Type _commandType)
    {
        this.name = _name;
        this.commandType = _commandType;
    }

    public bool hasCommand
    {
        get { return commandType != null; }
    }

    public void OnPress(GameActor actor = null)
    {
        actor.Execute(commandType, name);
    }

    public bool pressed
    {
        get { return Input.GetButton(name); }
    }

    public bool pressedDown
    {
        get { return Input.GetButtonDown(name); }
    }

    public bool pressedUp
    {
        get { return Input.GetButtonUp(name); }
    }

    public float axisValue
    {
        get { return Input.GetAxis(name); }
    }

    public float axisRawValue
    {
        get { return Input.GetAxisRaw(name); }
    }
}

public abstract class JoystickConfig
{
    public JoystickButton[] joystickButtons;

    public JoystickConfig()
    {
        Initialize();
    }

    protected abstract void Initialize();
}

public class KBMouseConfig : JoystickConfig
{
    JoystickButton escape = new JoystickButton("KB_ESC", typeof(CharacterCommand.Attack));
    JoystickButton leftctrl = new JoystickButton("KB_LCTRL", typeof(CharacterCommand.Attack));
    JoystickButton leftMouse = new JoystickButton("KB_LMOUSE", typeof(CharacterCommand.Attack));
    JoystickButton space = new JoystickButton("KB_SPACE", typeof(CharacterCommand.Attack));

    protected override void Initialize()
    {
        joystickButtons = new JoystickButton[] { escape, leftctrl, leftMouse, space };
    }
}

public class X360Config : JoystickConfig
{
    JoystickButton x360_a = new JoystickButton("X360_A", typeof(CharacterCommand.Attack));
    JoystickButton x360_RT = new JoystickButton("X360_RT", typeof(CharacterCommand.Attack));

    protected override void Initialize()
    {
        joystickButtons = new JoystickButton[] { x360_a, x360_RT };
    }
}