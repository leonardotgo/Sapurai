﻿/*
       Button gesture detection script
 *              by Leonardo Tagliaro
*/

using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class ButtonGesture : MonoBehaviour {

    /// <summary>
    /// Generic key holding detection function with event. 
    /// Event is activated WHILE the user is holding the key,
    /// after the user has held for targetTime seconds.
    /// NOTE: Holding the key any longer will produce no effect.
    /// </summary>
    /// <param name="key"></param>
    /// <param name="maxHoldTime"></param>
    /// <param name="action"></param>
    /// <returns></returns>
    public static IEnumerator HoldingKey(KeyCode key, float maxHoldTime, UnityAction action)
    {
        float keyHoldingTime = 0f;
        // Check if player is still holding key
        while (Input.GetKey(key))
        {
            keyHoldingTime += Time.deltaTime;
            if (keyHoldingTime >= maxHoldTime)
            {
                // If key is held for the target duration, execute the target function
                if (action != null)
                    action.Invoke();
                break;
            }
            yield return null;
        }

    }

    public static IEnumerator HoldingButton(string button, float maxHoldTime, UnityAction action)
    {
        float buttonHoldingTime = 0f;
        // Check if player is still holding key
        while (Input.GetButton(button))
        {
            buttonHoldingTime += Time.deltaTime;
            if (buttonHoldingTime >= maxHoldTime)
            {
                // If key is held for the target duration, execute the target function
                if (action != null)
                    action.Invoke();
                break;
            }
            yield return null;
        }

    }
}
