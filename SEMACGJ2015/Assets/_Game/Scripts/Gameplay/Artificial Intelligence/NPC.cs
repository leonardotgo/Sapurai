﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class NPC : Character {

    public CharacterBehaviour behaviour;

    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
 	    base.Update();
        behaviour.Update();
    }

    public virtual void SetBehaviour(CharacterBehaviour _behaviour)
    {
        if (this.actorState.GetType() != typeof(CharacterState.Dead))
        {
            if (behaviour != null)
                behaviour.OnExit();

            this.behaviour = _behaviour;

            if (behaviour != null)
                behaviour.OnEnter();
        }
        else
            behaviour = new CharacterBehaviour.Idle(this);
    }

    public virtual void Notify<T>(MonoBehaviour sender, params object[] args) where T : MonoBehaviour
    {
        if (typeof(T) == typeof(AITriggerCollider))
        {
            GameObject obj = args[0] as GameObject;
            if (obj.GetComponent<Character>() != null)
                OnCharacterEnterTrigger(sender.GetComponent<AITriggerCollider>(), obj.GetComponent<Character>());
        }
    }

    protected virtual void OnCharacterEnterTrigger(AITriggerCollider trigger, Character character)
    {

    }

    public override void OnDeath()
    {
        CharacterCommand.StopMovement stopMovement = new CharacterCommand.StopMovement();
        stopMovement.Execute(this);
        this.SetBehaviour(new CharacterBehaviour.Idle(this));
        base.OnDeath();
    }

    public void MoveTo(Vector3 position, float stopRange, UnityAction OnEndMovement = null)
    {
        var moveCommand = new CharacterCommand.Movement(position, stopRange, OnEndMovement);
        moveCommand.Execute(this);
    }

    public void SeekTarget(Transform target, float stopRange, UnityAction OnEndMovement = null)
    {
        var seekCommand = new CharacterCommand.SeekMovement(target, stopRange, OnEndMovement);
        seekCommand.Execute(this);
    }

    protected override void AllocateCommands()
    {
        base.AllocateCommands();
    }
}
