﻿using UnityEngine;
using System.Collections;

public class PotionItem : Item {

    public float healAmount = 30f;

    public override void ApplyEffect(Character target)
    {
        CombatEffect.Heal heal = new CombatEffect.Heal(healAmount);
        heal.Apply(target);
    }
}
