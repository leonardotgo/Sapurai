﻿using UnityEngine;
using System.Collections;

public class Item : MonoBehaviour {

    public LayerMask affectedCharacters;

    void OnTriggerEnter(Collider other)
    {
        if(LayerUtility.IsInLayerMask(other.gameObject.layer, affectedCharacters))
        {
            Character target = other.GetComponent<Character>();
            gameObject.SetActive(false);
            this.ApplyEffect(target);
        }
    }

    public virtual void ApplyEffect(Character target) { }
}
