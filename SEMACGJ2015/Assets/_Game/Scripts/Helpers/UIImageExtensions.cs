﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public static class UIImageExtensions {

    public static void SetColor(this Image image, Color target)
    {
        image.color = target;
    }

    public static void SetAlpha(this Image image, float alpha)
    {
        image.color = new Color(image.color.r, image.color.g, image.color.b, alpha);
    }

    public static void FadeColor(this Image image, Color target, float duration)
    {
        image.StartCoroutine(Fade(image, target, duration));
    }

    public static void FadeAlpha(this Image image, float targetAlpha, float duration)
    {
        image.StartCoroutine(Fade(image, new Color(image.color.r, image.color.g, image.color.b, targetAlpha), duration));
    }

    private static IEnumerator Fade(Image image, Color target, float duration)
    {
        Color original = image.color;
        float t = 0f;
        if (duration > 0f)
        {
            while (t < 0.99f)
            {
                image.color = Color.Lerp(original, target, t);
                t += 1f / duration * Time.deltaTime;
                yield return null;
            }
            image.color = target;
        }
        else
            image.color = target;
    }
}
