﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public static class UITextExtensions {

    public static void SetColor(this Text text, Color target)
    {
        text.color = target;
    }

    public static void SetAlpha(this Text text, float alpha)
    {
        text.color = new Color(text.color.r, text.color.g, text.color.b, alpha);
    }

    public static void FadeColor(this Text text, Color target, float duration)
    {
        text.StartCoroutine(Fade(text, target, duration));
    }

    public static void FadeAlpha(this Text text, float targetAlpha, float duration)
    {
        text.StartCoroutine(Fade(text, new Color(text.color.r, text.color.g, text.color.b, targetAlpha), duration));
    }

    private static IEnumerator Fade(Text UIText, Color target, float duration)
    {
        Color original = UIText.color;
        float t = 0f;
        if (duration > 0f)
        {
            while (t < 0.99f)
            {
                UIText.color = Color.Lerp(original, target, t);
                t += 1f / duration * Time.deltaTime;
                yield return null;
            }
            UIText.color = target;
        }
        else
            UIText.color = target;
    }
}
