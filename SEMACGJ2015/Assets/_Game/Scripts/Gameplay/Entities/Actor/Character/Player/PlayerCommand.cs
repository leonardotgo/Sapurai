﻿using UnityEngine;
using System.Collections;
using System;

public abstract class PlayerCommand : CharacterCommand
{
    public sealed new class Attack : CharacterCommand.Attack
    {
        public override void Execute(GameActor actor, params object[] args)
        {
            Player player = actor as Player;
            base.Execute(actor, args);
            player.weapon.OnUse();
        }
    }
}
