﻿using UnityEngine;
using System.Collections;

public abstract class CharacterAnimator : MonoBehaviour {

    public Character character;
    protected Animator animator;

    public virtual void Start()
    {
        animator = GetComponent<Animator>();
    }
}