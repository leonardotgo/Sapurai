﻿using UnityEngine;
using System.Collections;

public class LevelExit : MonoBehaviour {

	void OnTriggerEnter(Collider other)
    {
        GameController.SetNextLevel();
    }
}
