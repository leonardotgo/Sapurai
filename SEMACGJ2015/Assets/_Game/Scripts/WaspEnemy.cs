﻿using UnityEngine;
using System.Collections;

public class WaspEnemy : Enemy {

    protected override void Start()
    {
        base.Start();
        this.behaviour = new CharacterBehaviour.WalkRandomly(this, 2.5f, 0.25f);
        this.skeleton = new WaspSkeleton(this);
        this.SetColor(color);
    }

    public override void OnDeath()
    {
        base.OnDeath();
        skeleton.SetMovementTrigger("Death");
    }
}
