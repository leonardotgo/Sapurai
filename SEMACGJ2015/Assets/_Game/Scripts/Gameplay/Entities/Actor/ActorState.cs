﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class ActorState {

    public Dictionary<Type, Command> commands;
    protected GameActor actor;

    public ActorState(GameActor _actor)
    {
        commands = new Dictionary<Type, Command>();
        this.actor = _actor;
    }

    public abstract void OnEnter();
    public abstract void OnExit();

    public abstract void AllocateCommands();

    public virtual void Execute(Type commandType, params object[] args)
    {
        if (commands.ContainsKey(commandType))
            commands[commandType].Execute(actor, args);
    }

    public virtual void Execute<T>(params object[] args) where T : Command
    {
        if (commands.ContainsKey(typeof(T)))
            commands[typeof(T)].Execute(actor, args);
    }

    public virtual void Update() { }
}
