﻿using UnityEngine;
using System.Collections;

public abstract class Weapon : MonoBehaviour {

    public string weaponName = "Unnamed weapon";

    public Character wielder;

    public float damage = 10f;

    public bool hitOwner = false;
    public float knockbackStrength = 0f;
    public bool stun = false;

    public virtual void OnUse() { }

    public virtual void OnHit(Character target) { }

    public virtual void AnimationEvent(string message) { }
}
