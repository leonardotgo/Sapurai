﻿using UnityEngine;
using System.Collections;

public class BackgroundMusic : MonoBehaviour {

    private static BackgroundMusic instance;
    private static AudioSource audioSource
    {
        get { return instance.GetComponent<AudioSource>(); }
    }

    public static AudioClip _Music
    {
        get { return instance.music;  }
        set { instance.music = value; }
    }

    public AudioClip music;
    
	void Start () {
        instance = this;
        Play();
	}

    public static void Play()
    {
        if (instance.music != null)
        {
            audioSource.clip = _Music;
            audioSource.Play();
        }
    }

    
}
