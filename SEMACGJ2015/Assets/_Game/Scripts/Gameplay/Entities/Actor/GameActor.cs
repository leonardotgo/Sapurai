﻿using System;
using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public abstract class GameActor : MonoBehaviour
{
    protected Hashtable cachedStates = new Hashtable();
    protected ActorState actorState;

    public float speed = 5f;
    [HideInInspector] public Vector3 currentSpeed = Vector3.zero;

    public Vector3 xzSpeed
    {
        get { return Vector3.ProjectOnPlane(currentSpeed, Vector3.up); }
    }

    public void HandleMovement(Vector2 dir)
    {
        if (dir.magnitude > 0.1f || this.currentSpeed.magnitude > 0f)
        {
            Vector3 acceleration = dir.x * Vector3.right
                                   + dir.y * Vector3.forward;

            acceleration = acceleration.normalized * this.speed;

            this.currentSpeed = Vector3.Lerp(this.currentSpeed, new Vector3(acceleration.x, currentSpeed.y, acceleration.z), 1f);
        }
    }

    protected virtual void Awake()
    {
        this.AttachReferences();
    }

    public virtual void Execute(Type commandType, params object[] args)
    {
        actorState.Execute(commandType, args);
    }

    public void SetState(ActorState state)
    {
        if (actorState != null)
            actorState.OnExit();

        actorState = state;

        if (actorState != null)
            actorState.OnEnter();
    }

    public void SetState<T>() where T : ActorState
    {
        SetState(cachedStates[typeof(T)] as ActorState);
    }

    protected virtual void AttachReferences()
    {
        AllocateStates();
        AllocateCommands();
    }

    protected abstract void AllocateStates();

    protected virtual void AllocateCommands()
    {
        foreach (ActorState state in cachedStates.Values)
            state.AllocateCommands();
    }
}