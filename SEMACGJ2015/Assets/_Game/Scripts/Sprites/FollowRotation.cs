﻿using UnityEngine;
using System.Collections;

public class FollowRotation : MonoBehaviour {

    public Transform target;
	
	// Update is called once per frame
	void Update () {
        transform.rotation = Quaternion.Lerp(transform.rotation, target.rotation, 10f * Time.deltaTime);

	}
}
