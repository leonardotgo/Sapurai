﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainMenuController : MonoBehaviour {

    public static MainMenuController instance;
    public Image[] splashScreens;
    public Movie[] introMovies;
    public Movie finalIntro;
    public Movie menuLoop;
    public Canvas menu;

    IEnumerator Start()
    {
        foreach(Image splashScreen in splashScreens)
        {
            splashScreen.gameObject.SetActive(true);
            splashScreen.SetColor(Color.clear);
            splashScreen.FadeColor(Color.white, 1f);
            yield return new WaitForSeconds(3f);
            splashScreen.FadeColor(Color.clear, 1f);
            yield return new WaitForSeconds(1f);
            splashScreen.gameObject.SetActive(false);
        }

        foreach(Movie introMovie in introMovies)
        {
            introMovie.gameObject.SetActive(true);
            yield return new WaitForSeconds(introMovie.duration);
            ObjectFader.FadeAlpha(introMovie.gameObject, 0f, 1f);
            yield return new WaitForSeconds(1.1f);
            introMovie.gameObject.SetActive(false);
        }

        finalIntro.gameObject.SetActive(true);
        yield return new WaitForSeconds(finalIntro.duration);
        finalIntro.gameObject.SetActive(false);
        menu.gameObject.SetActive(true);

        menuLoop.gameObject.SetActive(true);
        while(true)
        {
            yield return new WaitForSeconds(menuLoop.duration);
            menuLoop.movie.Stop();
            menuLoop.movie.Play();
        }
    }

	public void OnPressPlay()
    {
        Application.LoadLevel(1);
    }
}
