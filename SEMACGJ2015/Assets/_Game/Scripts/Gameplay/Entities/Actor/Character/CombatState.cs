﻿using UnityEngine;
using System.Collections;

public class CombatState : CharacterState
{
    public CombatState(Character _character)
        : base(_character)
    {
        this.character = _character;
    }

    public override void OnEnter() { }
    public override void OnExit() { }
    public override void Update()
    {
        base.Update();
    }

    public class Attacking : CombatState
    {
        public Attacking(Character _character) : base(_character) { }


    }
}
