﻿using UnityEngine;
using System.Collections;

public class Credits : MonoBehaviour {

    void Start()
    {
        StartCoroutine(CreditsRoutine());
    }

	private IEnumerator CreditsRoutine()
    {
        RectTransform rectTransform = GetComponent<RectTransform>();
        Vector3 originalPosition = rectTransform.anchoredPosition3D;
        float time = 0f;
        while(time < 16f)
        {
            time += Time.deltaTime;
            rectTransform.anchoredPosition3D += Vector3.up * 37.5f * Time.deltaTime;
            yield return null;
        }
        rectTransform.anchoredPosition3D = originalPosition;
        this.transform.parent.gameObject.SetActive(false);
    }
}
