﻿using UnityEngine;
using System.Collections;

public class CharacterBodyPart : MonoBehaviour {

    public string type = "None";

    public GameObject[] graphics;

    public void SetColor(Color color)
    {
        foreach (GameObject sprite in graphics)
            ColorManager.SetObjectColor(sprite, color);
    }

    public void Flash(Color color, float duration)
    {
        if(graphics.Length > 0 && duration > 0f)
            StartCoroutine(FlashRoutine(color, duration));
    }

    private IEnumerator FlashRoutine(Color target, float duration)
    {
        float time = 0f;
        Color[] originalColors = new Color[graphics.Length]; 
        for(int i = 0; i < graphics.Length; i++)
            originalColors[i] = ColorManager.GetObjectColor(graphics[i]);

        while (time < duration / 2f)
        {
            for (int i = 0; i < graphics.Length; i++)
            {
                Color color = Color.Lerp(originalColors[i], target, time / (duration / 2f));
                ColorManager.SetObjectColor(graphics[i], color);
            }
            time += Time.deltaTime;
            yield return null;
        }

        time = 0f;

        while (time < duration)
        {
            for (int i = 0; i < graphics.Length; i++)
            {
                Color color = Color.Lerp(target, originalColors[i], time / (duration / 2f));
                ColorManager.SetObjectColor(graphics[i], color);
            }
            time += Time.deltaTime;
            yield return null;
        }
    }
}
