﻿using UnityEngine;
using System.Collections;

public class PlayerSpawn : MonoBehaviour {

    public static PlayerSpawn instance;

	// Use this for initialization
	void Awake () {
        instance = this;
	}

    public static void SetPlayerToSpawn()
    {
        Camera.main.transform.position += instance.transform.position - Player.self.transform.position;
        Player.self.transform.position = instance.transform.position;
        Player.self.transform.forward = instance.transform.forward;
    }
}
