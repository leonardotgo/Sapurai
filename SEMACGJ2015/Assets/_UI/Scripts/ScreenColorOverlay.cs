﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;

public class ScreenColorOverlay : MonoBehaviour {

    public static ScreenColorOverlay instance;
    private bool flashing = false;

    private Image image
    {
        get { return GetComponent<Image>(); }
    }

    void Start()
    {
        flashing = false;
        instance = this;
    }

    public static void SetColor(Color target)
    {
        instance.image.SetColor(target);
    }

    public static void FadeColor(Color target, float duration)
    {
        instance.image.FadeColor(target, duration);
    }

	public static void Flash(Color color, float duration, UnityAction OnEndFlash = null)
    {
        if (!instance.flashing)
        {
            instance.flashing = true;
            instance.StartCoroutine(FlashRoutine(instance.image, color, duration, OnEndFlash));
        }
    }

    private static IEnumerator FlashRoutine(Image instanceImage, Color targetColor, float duration, UnityAction OnEndFlash)
    {
        Color original = instanceImage.color;
        instanceImage.FadeColor(targetColor, duration / 2f);
        yield return new WaitForSeconds(duration / 2f + 0.05f);
        instanceImage.FadeColor(original, duration / 2f);
        yield return new WaitForSeconds(duration / 2f + 0.05f);
        instanceImage.SetColor(original);
        if (OnEndFlash != null)
            OnEndFlash.Invoke();
        instance.flashing = false;
    }
}
