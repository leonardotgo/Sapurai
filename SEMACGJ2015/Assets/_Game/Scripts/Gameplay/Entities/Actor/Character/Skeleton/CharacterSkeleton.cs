﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Collections;

public class CharacterSkeleton
{

    protected Character character;

    protected CharacterAnimator[] animators;

    protected Dictionary<string, CharacterBodyPart> bodyParts;

    protected bool flashing = false;

    public CharacterSkeleton(Character _character)
    {
        this.character = _character;
        this.animators = new CharacterAnimator[0];
        this.flashing = false;
        InitializeBodyParts();
        InitializeAnimators();
    }

    public virtual void SetColor(Color color)
    {
        if (bodyParts != null)
        {
            CharacterBodyPart[] bps = bodyParts.Values.ToArray();
            foreach (CharacterBodyPart bodyPart in bps)
                bodyPart.SetColor(color);
        }
    }

    public virtual void Flash(Color color, float duration)
    {
        if (!flashing)
        {
            if (bodyParts != null)
            {
                this.flashing = true;
                CharacterBodyPart[] bps = bodyParts.Values.ToArray();
                foreach (CharacterBodyPart bodyPart in bps)
                    bodyPart.Flash(color, duration);
                ActionHelper.PerformDelayedAction(duration + 0.1f, () => { this.flashing = false; } );
            }
        }
    }

    public void SetMovementTrigger(string trigger)
    {
        foreach (CharacterAnimator anim in animators)
            if (anim is MovementAnimator)
            {
                MovementAnimator animator = anim as MovementAnimator;
                animator.SetTrigger(trigger);
            }
    }

    public void SetMovementSpeed(float speed)
    {
        foreach (CharacterAnimator anim in animators)
            if (anim is MovementAnimator)
            {
                MovementAnimator animator = anim as MovementAnimator;
                animator.SetSpeed(speed);
            }
    }

    public void SetMovementDirection(Vector2 dir)
    {
        foreach (CharacterAnimator anim in animators)
            if (anim is MovementAnimator)
            {
                MovementAnimator animator = anim as MovementAnimator;
                animator.SetDirection(dir);
            }
    }

    public void SetAttackTrigger(string trigger = "Attack")
    {
        foreach (CharacterAnimator anim in animators)
            if (anim is CombatAnimator)
            {
                CombatAnimator animator = anim as CombatAnimator;
                animator.SetAttackTrigger(trigger);
            }
    }

    public void Update()
    {
        this.SetMovementSpeed(character.xzSpeed.magnitude);

        Vector3 movementDir = character.xzSpeed.normalized;
        float dir_x = Vector3.Dot(movementDir, character.transform.right);
        float dir_z = Vector3.Dot(movementDir, character.transform.forward);
        Vector2 dir = new Vector2(dir_x, dir_z);
        this.SetMovementDirection(dir);
    }

    public virtual void InitializeBodyParts()
    {
        bodyParts = new Dictionary<string, CharacterBodyPart>();
        FindBodyPartsRecursively(character.transform);
    }

    public virtual void InitializeAnimators()
    {
        FindAnimatorsRecursively(character.transform);
    }

    public virtual void FindBodyPartsRecursively(Transform transform)
    {
        foreach (Transform child in transform)
        {
            if (child.GetComponent<CharacterBodyPart>() != null)
            {
                CharacterBodyPart bp = child.GetComponent<CharacterBodyPart>();
                if(!bodyParts.ContainsKey(bp.type))
                    bodyParts.Add(bp.type, bp);
            }

            FindBodyPartsRecursively(child);
        }
    }

    public virtual void FindAnimatorsRecursively(Transform transform)
    {
        foreach (Transform child in transform)
        {
            if (child.GetComponent<CharacterAnimator>() != null)
            {
                CharacterAnimator animator = child.GetComponent<CharacterAnimator>();

                List<CharacterAnimator> anims = animators.ToList();
                anims.Add(animator);
                animators = anims.ToArray();
            }

            FindAnimatorsRecursively(child);
        }
    }
}

public class HumanoidSkeleton : CharacterSkeleton
{

    protected new Dictionary<HumanoidBodyPart.BPType, HumanoidBodyPart> bodyParts;

    public HumanoidSkeleton(Character _character) : base(_character) { }

    public override void InitializeBodyParts()
    {
        bodyParts = new Dictionary<HumanoidBodyPart.BPType, HumanoidBodyPart>();
        FindBodyPartsRecursively(character.transform);
    }

    public override void FindBodyPartsRecursively(Transform transform)
    {
        foreach (Transform child in transform)
        {
            if (child.GetComponent<HumanoidBodyPart>() != null)
            {
                HumanoidBodyPart bp = child.GetComponent<HumanoidBodyPart>();
                if (!bodyParts.ContainsKey(bp.hType))
                    bodyParts.Add(bp.hType, bp);
            }

            FindBodyPartsRecursively(child);
        }
    }

    public override void Flash(Color color, float duration)
    {
        if (!flashing)
        {
            if (bodyParts != null)
            {
                flashing = true;
                HumanoidBodyPart[] bps = bodyParts.Values.ToArray();
                foreach (HumanoidBodyPart bodyPart in bps)
                    bodyPart.Flash(color, duration);
                ActionHelper.PerformDelayedAction(duration + 0.1f, () => { flashing = false; });
            }
        }
    }
}

public class WaspSkeleton : CharacterSkeleton 
{

    public WaspSkeleton(Character _character) : base(_character) { }

    public override void SetColor(Color color)
    {
        bodyParts["Body"].SetColor(color);
    }
}