﻿using UnityEngine;
using System.Collections;

public class CombatAnimator : CharacterAnimator
{
    public void SetAttackTrigger(string trigger)
    {
        animator.SetTrigger(trigger);
    }
}