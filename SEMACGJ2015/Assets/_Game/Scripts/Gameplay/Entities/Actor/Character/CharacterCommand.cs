﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System;

public abstract class CharacterCommand : Command
{
    public class Movement : CharacterCommand
    {
        private Vector3 targetPos;
        private float stopRange;
        private UnityAction onEnd;

        public Movement(Vector3 _targetPos, float _stopRange, UnityAction _onEnd = null)
        {
            this.targetPos = _targetPos;
            this.stopRange = _stopRange;
            this.onEnd = _onEnd;
        }

        public override void Execute(GameActor actor, params object[] args)
        {
            Character character = actor as Character;
            character.SetMovementState(new MovementState.MovingToPosition(character, targetPos, stopRange, onEnd));
        }
    }

    public class SeekMovement : CharacterCommand
    {
        Transform target;
        float stopRange;
        UnityAction onEnd;

        public SeekMovement(Transform _target, float _stopRange, UnityAction _onEnd = null)
        {
            this.target = _target;
            this.stopRange = _stopRange;
            this.onEnd = _onEnd;
        }

        public override void Execute(GameActor actor, params object[] args)
        {
            Character character = actor as Character;

            character.SetMovementState(new MovementState.SeekingTarget(character, target, stopRange, onEnd));
        }
    }

    public class FleeMovement : CharacterCommand
    {
        Transform target;
        float stopRange;
        UnityAction onEnd;

        public FleeMovement(Transform _target, float _stopRange, UnityAction _onEnd = null)
        {
            this.target = _target;
            this.stopRange = _stopRange;
            this.onEnd = _onEnd;
        }

        public override void Execute(GameActor actor, params object[] args)
        {
            Character character = actor as Character;

            character.SetMovementState(new MovementState.AvoidingTarget(character, target, stopRange, onEnd));
        }
    }

    public class StopMovement : CharacterCommand
    {
        public override void Execute(GameActor actor, params object[] args)
        {
            Character character = actor as Character;
            character.SetMovementState(new MovementState.Idle(character));
        }
    }

    public class Attack : CharacterCommand
    {
        public override void Execute(GameActor actor, params object[] args)
        {
            Character character = actor as Character;
            character.skeleton.SetAttackTrigger();
        }
    }
}
