﻿using UnityEngine;
using System.Collections;

public class TopDownCamera : MonoBehaviour {

    public Transform target;

    private float shakeStrength = 0f;
    private Vector3 offset;
    private BoxCollider boundingBox;

    void Start()
    {
        offset = transform.position - target.position;
        boundingBox = GameObject.FindGameObjectWithTag("CameraBounds").GetComponent<BoxCollider>();
    }

    void Update()
    {
        Vector3 targetPos = target.position;
        if (shakeStrength > 0.01f)
        {
            float[] random = new float[3] { Random.Range(-shakeStrength / 2f, shakeStrength / 2f),
                                        Random.Range(-shakeStrength / 2f, shakeStrength / 2f),
                                        Random.Range( -shakeStrength / 2f, shakeStrength / 2f) };

            Vector3 screenShakePos = new Vector3(random[0], random[1], random[2]);

            shakeStrength = Mathf.Lerp(shakeStrength, 0f, 5f * Time.deltaTime);
            targetPos += screenShakePos;
        }
        targetPos = ClampPositionToCameraBounds(targetPos);
        transform.position = Vector3.Lerp(transform.position, targetPos + offset, 10f * Time.deltaTime);
    }

    public void ShakeCamera(float strength)
    {
        if (strength > shakeStrength)
            shakeStrength = strength;
    }

    private Vector3 ClampPositionToCameraBounds(Vector3 targetPos)
    {
        if (boundingBox != null)
        {
            Vector3 currentOffset = targetPos - transform.position;

            float boundTopDist = Mathf.Abs(boundingBox.bounds.max.y - transform.position.y);

            Vector3 cameraMin = Camera.main.ScreenToWorldPoint(Camera.main.ViewportToScreenPoint(Vector3.forward * boundTopDist));
            cameraMin = cameraMin + currentOffset;

            Vector3 cameraMax = Camera.main.ScreenToWorldPoint(Camera.main.ViewportToScreenPoint(new Vector3(1f, 1f, boundTopDist)));
            cameraMax = cameraMax + currentOffset;

            float max_x = boundingBox.bounds.max.x;
            float max_z = boundingBox.bounds.max.z;
            float min_x = boundingBox.bounds.min.x;
            float min_z = boundingBox.bounds.min.z;

            if (cameraMax.x > max_x)
                targetPos.Set(targetPos.x + max_x - cameraMax.x, targetPos.y, targetPos.z);
            else if (cameraMin.x < min_x)
                targetPos.Set(targetPos.x + min_x - cameraMin.x, targetPos.y, targetPos.z);

            if (cameraMax.z > max_z)
                targetPos.Set(targetPos.x, targetPos.y, targetPos.z + max_z - cameraMax.z);
            else if (cameraMin.z < min_z)
                targetPos.Set(targetPos.x, targetPos.y, targetPos.z + min_z - cameraMin.z);

        }

        return targetPos;
    }
}
