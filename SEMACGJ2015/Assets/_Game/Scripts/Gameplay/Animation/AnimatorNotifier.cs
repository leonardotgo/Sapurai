﻿using UnityEngine;
using System.Collections;

public class AnimatorNotifier : MonoBehaviour {

    public GameObject target;

    public void Notify(string message)
    {
        target.SendMessage("AnimationEvent", message);
    }
}
