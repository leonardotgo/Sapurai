﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameController : MonoBehaviour {

    public static GameController instance;
    public LevelNumberText levelNumberText;
    public GameOverText gameOverText;
    public LevelGenerator levelGenerator;
    public int currentLevel;

    public int enemiesToKill;

    // Use this for initialization
    void Start() {
        instance = this;
        currentLevel = 1;
        StartLevel(currentLevel);
    }

    public static void LoadLevel(int level)
    {
        instance.levelGenerator.GenerateMap(level);
    }

    public static void OnKillEnemy()
    {
        instance.enemiesToKill--;
        if (instance.enemiesToKill <= 0)
            instance.levelGenerator.exit.GetComponent<Collider>().enabled = false;
    }

    public static void SetNextLevel()
    {
        instance.StartCoroutine(instance.EndLevelRoutine());
    }

    public static void StartLevel(int level)
    {
        if (level == 1)
            ScreenColorOverlay.SetColor(Color.black);

        instance.StartCoroutine(instance.StartLevelRoutine(level));
    }

    private IEnumerator EndLevelRoutine()
    {
        currentLevel += 1;
        ScreenColorOverlay.FadeColor(Color.black, 1f);
        yield return new WaitForSeconds(1f);
        StartLevel(currentLevel);
    }

    private IEnumerator StartLevelRoutine(int level)
    {
        GameController.LoadLevel(level);
        PlayerSpawn.SetPlayerToSpawn();

        ScreenColorOverlay.FadeColor(Color.clear, 2f);
        yield return new WaitForSeconds(2f);

        DisplayLevelNumberText();
        yield return new WaitForSeconds(4f);
        levelNumberText.gameObject.SetActive(false);
    }

    public static void DisplayLevelNumberText()
    {
        instance.levelNumberText.UIText.text = "Floor " + instance.currentLevel;
        instance.levelNumberText.gameObject.SetActive(true);
    }

    public static void OnExitLevel()
    {

    }

    public static void OnGameOver()
    {
        instance.StartCoroutine(instance.GameOverRoutine());
    }

    private IEnumerator GameOverRoutine()
    {
        yield return new WaitForSeconds(2f);
        ScreenColorOverlay.FadeColor(Color.black, 2f);
        yield return new WaitForSeconds(2f);
        yield return gameOverText.StartCoroutine(gameOverText.GameOverTextRoutine());
        Application.LoadLevel(0);
    }
}
