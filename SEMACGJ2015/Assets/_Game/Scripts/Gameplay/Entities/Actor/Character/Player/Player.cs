﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class Player : Character {

    public static Player self;
    public Text playerHealthText;

    protected override void Awake()
    {
        // Attach singleton reference
        Player.self = this;

        base.Awake();
        skeleton = new HumanoidSkeleton(this);

        this.SetMovementState(new MovementState.PlayerControlledMovement(this));
        CombatEffect.DamageOverTime poison = new CombatEffect.DamageOverTime(-1f, 3f, 0f);
        poison.Apply(this);
    }

    protected override void AttachReferences ()
	{
        base.AttachReferences();

        Player.self = this;
	}

    public override void Flash(Color color, float duration)
    {
        skeleton.Flash(color, duration);
    }

    public override void OnDeath()
    {
        base.OnDeath();
        this.SetMovementState(new MovementState.Dead(this));
        GameController.OnGameOver();
    }

    public override void ChangeHealth(float delta)
    {
        this.SetHealth(this.currentHealth + delta);

        Color tgtColor = (delta < 0f) ? new Color(0.75f, 0.5f, 1.0f, 0.75f) : new Color(0f, 1f, 0f, 0.75f);
        this.Flash(tgtColor, 0.3f);

        if(playerHealthText != null)
            playerHealthText.text = ((int)this.currentHealth).ToString();

        if(Mathf.Abs(delta) > 3f)
            ScreenColorOverlay.Flash(new Color(tgtColor.r, tgtColor.g, tgtColor.b, 0.1f), 0.2f);
            
    }

    protected override void AllocateStates()
    {
        cachedStates.Add(typeof(CharacterState.Default), new PlayerState.Default(this));
        cachedStates.Add(typeof(CharacterState.Dead), new CharacterState.Dead(this));
    }
}


