﻿using System;
using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class InputHandler : MonoBehaviour {

    delegate void InputHandlerCallback(params object[] args);

    private   GameActor targetRef = null;
    protected GameActor inputTarget
    {
        get
        {
            if (targetRef == null)
                targetRef = GetComponent<GameActor>();
            return targetRef;
        }
        set { targetRef = value; }
    }

    public static JoystickConfig[] joystickConfigs = new JoystickConfig[] { new KBMouseConfig(), new X360Config() };

	// Update is called once per frame
	void Update ()
    {
        foreach (JoystickConfig currentConfig in joystickConfigs)
        {
            foreach (JoystickButton button in currentConfig.joystickButtons)
            {
                if (button.pressedDown)
                    if (inputTarget != null && button.commandType != null)
                        button.OnPress(inputTarget);
            }
        }
	}

    public static Vector3 GetMouseWorldPoint(int layerMask)
    {
        Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;
        Physics.Raycast(mouseRay, out hitInfo, layerMask);
        return new Vector3(hitInfo.point.x, Player.self.transform.position.y, hitInfo.point.z);
    }

    static void AssignCommandToButton(params object[] args)
    {
        JoystickButton button = args[0] as JoystickButton;
        Type command = args[1] as Type;

        button.commandType = command;
    }
}
