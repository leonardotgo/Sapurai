﻿using UnityEngine;
using System.Collections;

public class SpiderEnemy : Enemy
{
    public enum EnterVisionBehaviour
    {
        Ignore,
        Avoid,
        Attack
    }

    public AITriggerCollider visionCircle;
    public EnterVisionBehaviour onEnterVision;
    public float attackRange = 1.1f;

    protected override void Start()
    {
        base.Start();
        this.SetBehaviour(new CharacterBehaviour.WalkRandomly(this, 4f, 1.5f));
        this.SetColor(color);
    }

    public override void Notify<T>(MonoBehaviour sender, params object[] args)
    {
        base.Notify<T>(sender, args);
        System.Type notificationType = typeof(T);

        if (notificationType == typeof(AITriggerCollider))
        {
            GameObject obj = args[0] as GameObject;
            bool enter = (bool)args[1];

            if (enter)
                OnEnterVisionCircle(obj);
            else
                OnExitVisionCircle(obj);
        }
    }

    public void OnEnterVisionCircle(GameObject obj)
    {
        
        if (obj.GetComponent<Character>() != null)
        {
            Character target = obj.GetComponent<Character>();
            if(onEnterVision == EnterVisionBehaviour.Attack)
                this.SetBehaviour(new CharacterBehaviour.AttackTarget(this, target, this.attackRange));
            else if(onEnterVision == EnterVisionBehaviour.Avoid)
            {
                float avoidRange = visionCircle.GetComponent<SphereCollider>().radius * 2f;
                this.SetBehaviour(new CharacterBehaviour.AvoidTarget(this, target, avoidRange));
             }
        }
    }

    public void OnExitVisionCircle(GameObject obj)
    {
        if (obj.GetComponent<Character>() != null)
        {
            this.SetBehaviour(new CharacterBehaviour.WalkRandomly(this, 4f, 1.5f));
        }
    }
}
