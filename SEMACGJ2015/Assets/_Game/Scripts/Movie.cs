﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Movie : MonoBehaviour {

    public MovieTexture movie
    {
        get {
            if(GetComponent<MeshRenderer>() != null)
                return GetComponent<MeshRenderer>().material.mainTexture as MovieTexture;
            else
                return GetComponent<Image>().material.mainTexture as MovieTexture;
        }
    }

    public float duration = 1f;

	// Use this for initialization
	void Start () {
        if(GetComponent<MeshRenderer>() != null)
        {
            float quadHeight = Camera.main.orthographicSize * 2.0f;
            float quadWidth = quadHeight * Screen.width / Screen.height;
            transform.localScale = new Vector3(quadWidth, quadHeight, 1);
        }
        movie.Play();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
