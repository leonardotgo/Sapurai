﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(AudioSource))]
public class Character : GameActor {

    [Range(0f, 500f)]
    public float maxHealth = 50f;
    protected float currentHealth;

    [System.NonSerialized]
    public CharacterSkeleton skeleton;
    public MovementState movementState;

    public AudioClip deathSound;
    public Weapon weapon;

    public bool unKnockable = false;

    private CharacterController charControllerRef = null;
    protected CharacterController controller
    {
        get
        {
            if (charControllerRef == null)
                charControllerRef = GetComponent<CharacterController>();

            return charControllerRef;
        }
    }

    public bool isGrounded
    {
        get { return controller.isGrounded; }
    }

    public bool isDead
    {
        get { return actorState.GetType() == typeof(CharacterState.Dead); }
    }

    public virtual void SetColor(Color color)
    {
        skeleton.SetColor(color);
    }

    protected override void Awake()
    {
        base.Awake();

        skeleton = new CharacterSkeleton(this);

        this.SetState<CharacterState.Default>();
        this.SetMovementState(new MovementState.Idle(this));
    }

    public void PlaySound(AudioClip sound)
    {
        AudioSource audioSource = GetComponent<AudioSource>();
        audioSource.PlayOneShot(sound);
    }

    protected virtual void Start()
    {
        currentHealth = maxHealth;
    }

    protected virtual void Update()
    {
        actorState.Update();
        movementState.Update();
        this.Move(this.currentSpeed * Time.deltaTime);
        skeleton.Update();
    }

    public void SetMovementState(MovementState state)
    {
        if (movementState != null)
            movementState.OnExit();

        movementState = state;

        if (movementState != null)
            movementState.OnEnter();
    }

    public virtual void Flash(Color color, float duration)
    {
        Color originalColor = ColorManager.GetObjectColor(GetComponentInChildren<MeshRenderer>().gameObject);
        ObjectFader.OnObjectEndFade flashBack = () => ObjectFader.FadeColorHierarchy(gameObject, originalColor, duration / 2f);
        ObjectFader.FadeColorHierarchy(gameObject, color, duration / 2f, "_Color", flashBack);
    }

    public virtual void AnimationEvent(string trigger)
    {

    }

    public virtual void SetHealth(float value)
    {
        this.currentHealth = Mathf.Clamp(value, 0f, maxHealth);
        if (currentHealth <= 0f)
        {
            if (actorState.GetType() != typeof(CharacterState.Dead))
                this.OnDeath();
        }
    }

    public virtual void ChangeHealth(float delta)
    {
        this.SetHealth(this.currentHealth + delta);

        Color tgtColor = (delta < 0f) ? new Color(1.5f, 0.5f, 0.5f, 0.75f) : new Color(0.5f, 1.5f, 0.5f, 0.75f);
        this.Flash(tgtColor, 0.3f);
    }

    public virtual void OnDeath()
    {
        this.SetState<CharacterState.Dead>();
        if (deathSound != null)
            PlaySound(deathSound);
    }

    public void Move(Vector3 offset)
    {
        controller.Move(offset);
    }

    protected override void AllocateStates()
    {
        cachedStates.Add(typeof(CharacterState.Default), new CharacterState.Default(this));
        cachedStates.Add(typeof(CharacterState.Dead),    new CharacterState.Dead(this));
    }
}
