﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameOverText : MonoBehaviour {

    public Text text
    {
        get { return GetComponent<Text>(); }
    }

    void Start () {
        text.SetAlpha(0f);
    }

    public IEnumerator GameOverTextRoutine()
    {
        Color originalColor = text.color;
        text.text = "YOU ARE DEAD DESU";
        text.SetAlpha(0f);
        text.FadeAlpha(1f, 1f);
        yield return new WaitForSeconds(3.5f);
        text.FadeAlpha(0f, 1f);
        yield return new WaitForSeconds(1f);
        text.text = "GAME OVER";
        text.FadeAlpha(1f, 1f);
        yield return new WaitForSeconds(4f);
        text.FadeAlpha(0f, 1f);
        yield return new WaitForSeconds(1f);
        text.SetColor(Color.white);
        text.text = "Thank you for playing!";
        text.FadeAlpha(1f, 1f);
        yield return new WaitForSeconds(3f);
        text.FadeAlpha(0f, 1f);
        text.SetColor(originalColor);
        yield return new WaitForSeconds(1f);
    }
}
