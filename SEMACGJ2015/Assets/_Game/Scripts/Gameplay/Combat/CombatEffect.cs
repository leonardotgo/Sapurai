﻿using UnityEngine;
using System.Collections;
using System;

public abstract class CombatEffect {

    public abstract void Apply(Character target);

    public sealed class Heal : CombatEffect
    {
        float healAmount;

        public Heal(float amount)
        {
            healAmount = amount;
        }

        public override void Apply(Character target)
        {
            target.ChangeHealth(healAmount);
        }
    }

    public sealed class DamageOverTime : CombatEffect
    {
        float amount;
        float interval;
        float duration;

        public DamageOverTime(float _amount, float _interval, float _duration)
        {
            this.amount = _amount;
            this.interval = _interval;
            this.duration = _duration;
        }

        public override void Apply(Character target)
        {
            target.StartCoroutine(DamageOverTimeRoutine(target));
        }

        private IEnumerator DamageOverTimeRoutine(Character target)
        {
            float time = 0f;
            float currentInterval = 0f;
            if (duration == 0f)
            {
                while (!target.isDead)
                {
                    currentInterval += Time.deltaTime;
                    if (currentInterval > interval)
                    {
                        target.ChangeHealth(amount);
                        currentInterval = 0f;
                    }
                    yield return null;
                }
            }
            else
            {
                
                while (time < duration)
                {
                    time += Time.deltaTime;
                    currentInterval += Time.deltaTime;
                    if(currentInterval > interval)
                    {
                        target.ChangeHealth(amount);
                        currentInterval = 0f;
                    }
                    yield return null;
                }
            }
        }
    }

    public sealed class Knoback : CombatEffect
    {
        float force = 0f;
        float forceDamp = 10f;

        Vector3 direction = Vector3.zero;

        public Knoback(Vector3 _direction, float _strength = 0f)
        {
            force = _strength;
            direction = _direction;
        }

        public override void Apply(Character target)
        {
            if(!target.unKnockable)
                target.StartCoroutine(KnockbackRoutine(target, direction, force));
        }

        private IEnumerator KnockbackRoutine(Character target, Vector3 dir, float force)
        {
            MovementState movementState = target.movementState;
            target.SetMovementState(new MovementState.Knockback(target));
            while(force > 0f)
            {
                force -= forceDamp * Time.deltaTime;
                target.Move(dir * force * Time.deltaTime);
                yield return null;
            }
            if(movementState.GetType() != typeof(MovementState.Knockback))
                target.SetMovementState(movementState);
            
        }
    }
}
