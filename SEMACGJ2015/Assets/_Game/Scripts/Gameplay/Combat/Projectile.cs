﻿using UnityEngine;
using System.Collections;
using System;

public abstract class Projectile : GameActor {

    public void Update()
    {
        transform.position += currentSpeed * Time.deltaTime;
        transform.rotation = Quaternion.LookRotation(currentSpeed, Vector3.up);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Character>() != null)
        {
            Character target = other.GetComponent<Character>();
            this.OnHit(target);
        }
    }

    protected virtual void Launch(Vector3 origin, float _speed, Vector3 direction)
    {
        Projectile projectile = GameObject.Instantiate<Projectile>(this);
        projectile.transform.position = origin;
        projectile.currentSpeed = direction * _speed;
        projectile.gameObject.SetActive(true);
    }

    protected abstract void OnHit(Character target);

    protected override void AllocateStates()
    {
    }

    public override void Execute(Type commandType, params object[] args)
    {
        base.Execute(commandType, args);
    }
}
