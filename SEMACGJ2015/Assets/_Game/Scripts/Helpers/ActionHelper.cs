﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;


public class ActionHelper : MonoBehaviour {

    private static MonoBehaviour monoBehaviour = null;
    public static MonoBehaviour mono
    {
        get 
        {
            if (!monoBehaviour)
                monoBehaviour = (new GameObject("ActionHelper_Routiner", typeof(ActionHelper))).GetComponent<ActionHelper>();

            return monoBehaviour;
        }
    }

    public static void PerformNextFrameAction(UnityAction action)
    {
        mono.StartCoroutine(ActionNextFrameCoroutine(action));
    }

	public static void PerformDelayedAction(float delay, UnityAction action)
    {
        mono.StartCoroutine(ActionDelayCoroutine(delay,action));
    }

    private static IEnumerator ActionNextFrameCoroutine(UnityAction action)
    {
        yield return null;

        if(action != null)
            action.Invoke();
    }

    private static IEnumerator ActionDelayCoroutine(float delay, UnityAction action)
    {
        yield return new WaitForSeconds(delay);

        if(action != null)
            action.Invoke();
    }
}
