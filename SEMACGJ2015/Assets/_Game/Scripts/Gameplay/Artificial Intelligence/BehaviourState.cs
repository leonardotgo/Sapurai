﻿using UnityEngine;
using System.Collections;

public class CharacterBehaviour : CharacterState {

	public CharacterBehaviour(Character _character) : base(_character) { }

    public class Idle : CharacterBehaviour
    {
        public Idle(Character _character) : base(_character) { }
    }

    public class WalkRandomly : CharacterBehaviour
    {
        private float distance;
        private float waitTime;

        private bool makingRandomMovement;
        private float waitedTime;

        public WalkRandomly(Character _character, float _distance, float _waitTime) 
            : base(_character)
        {
            this.distance = _distance;
            this.waitTime = _waitTime;
        }

        public override void OnEnter()
        {
            base.OnEnter();
            makingRandomMovement = false;
            waitedTime = 0f;
        }

        public override void Update()
        {
            base.Update();
            if (!makingRandomMovement)
            {
                waitedTime += Time.deltaTime;
                if (waitedTime > waitTime)
                {
                    waitedTime = 0f;
                    MakeRandomMovement();
                }
            }
        }

        private void MakeRandomMovement()
        {
            float possibleDist = distance;

            Vector3 randomDir = Quaternion.AngleAxis(Random.Range(45f, 315f), Vector3.up) * character.transform.forward;
            RaycastHit[] hitInfos = Physics.RaycastAll(character.transform.position, randomDir, distance);
            foreach (RaycastHit hitInfo in hitInfos)
                if (hitInfo.distance < possibleDist && hitInfo.collider.gameObject != character.gameObject)
                    possibleDist = hitInfo.distance;

            CharacterCommand.Movement movement = new CharacterCommand.Movement(character.transform.position + randomDir * possibleDist, 1f, () => { makingRandomMovement = false; });
            movement.Execute(character);
            makingRandomMovement = true;
        }
    }

    public class AvoidTarget : CharacterBehaviour
    {
        protected Character target;
        protected float avoidRange;

        public AvoidTarget(Character _character, Character _target, float _avoidRange) 
            : base(_character)
        {
            this.target = _target;
            this.avoidRange = _avoidRange;
        }

        public override void OnEnter()
        {
            base.OnEnter();
            CharacterCommand.FleeMovement flee = new CharacterCommand.FleeMovement(target.transform, avoidRange);
            flee.Execute(character);
        }
    }

    public class FollowTarget : CharacterBehaviour
    {
        public FollowTarget(Character _character) : base(_character) { }
    }

    public class AttackTarget : CharacterBehaviour
    {
        private Character target;
        private float attackRange;

        public AttackTarget(Character _character, Character _target, float _attackRange) : base(_character)
        {
            this.target = _target;
            this.attackRange = _attackRange;
        }

        public override void OnEnter()
        {
            base.OnEnter();
            CharacterCommand.SeekMovement seek = new CharacterCommand.SeekMovement(target.transform, attackRange);
            seek.Execute(character);
        }

        public override void Update()
        {
            base.Update();
            if(Vector3.Distance(character.transform.position, target.transform.position) < attackRange)
            {
                CharacterCommand.Attack attack = new CharacterCommand.Attack();
                attack.Execute(character);
            }
        }
    }
}
