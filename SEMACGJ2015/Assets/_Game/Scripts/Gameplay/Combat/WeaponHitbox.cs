﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class WeaponHitbox : MonoBehaviour {

    public enum ColliderHitType
    {
        Enter,
        Exit,
        Stay
    }

    public Weapon weapon;
    public LayerMask hittableCharacters;
    public ColliderHitType hitWhen = ColliderHitType.Enter;
    public float damageInterval = 0f;

    public UnityEvent onHit;

    private bool canApply = true;

    void OnTriggerEnter(Collider other)
    {
        if (hitWhen == ColliderHitType.Enter)
            HitCheck(other);
    }

    void OnTriggerExit(Collider other)
    {
        if (hitWhen == ColliderHitType.Exit)
            HitCheck(other);
    }

    void OnTriggerStay(Collider other)
    {
        if (hitWhen == ColliderHitType.Stay)
            HitCheck(other);
    }

    public void HitCheck(Collider other)
    {
        if (LayerUtility.IsInLayerMask(other.gameObject.layer, hittableCharacters))
        {
            if (other.gameObject.GetComponent<Character>() != null && canApply)
            {
                Character target = other.gameObject.GetComponent<Character>();

                if (target != weapon.wielder || weapon.hitOwner)
                { 
                    if (!target.isDead && !weapon.wielder.isDead)
                    {
                        weapon.OnHit(target);
                        onHit.Invoke();
                        if (damageInterval > 0f)
                        {
                            canApply = false;
                            ActionHelper.PerformDelayedAction(damageInterval, () => { canApply = true; });
                        }
                    }
                }
            }
        }
    }
}
