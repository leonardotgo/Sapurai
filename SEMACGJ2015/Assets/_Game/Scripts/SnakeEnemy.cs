﻿using UnityEngine;
using System.Collections;

public class SnakeEnemy : Enemy {

	protected override void Start () {
        base.Start();
        this.behaviour = new CharacterBehaviour.WalkRandomly(this, 8f, 1.5f);
        this.SetColor(color);
	}
}
