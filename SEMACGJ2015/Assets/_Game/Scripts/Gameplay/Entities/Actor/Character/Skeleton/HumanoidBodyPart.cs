﻿using UnityEngine;
using System.Collections;

public class HumanoidBodyPart : CharacterBodyPart {

    public enum BPType
    {
        None,
        Head,
        Chest,
        HandsBase,
        LeftHand,
        RightHand,
        FeetBase,
        LeftFoot,
        RightFoot
    }

    public BPType hType = BPType.None;
}
