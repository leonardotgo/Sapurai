﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class MovementState : CharacterState {

    public MovementState(Character _character) : base(_character) { }

    public override void Update()
    {
        base.Update();
        this.UpdateRotation();
    }

    public virtual void UpdateRotation()
    {
        if (character.xzSpeed.magnitude > 0.25f)
        {
            character.transform.rotation = Quaternion.Lerp(character.transform.rotation, Quaternion.LookRotation(character.xzSpeed.normalized, Vector3.up), 15f * Time.deltaTime);
        }
    }

    public sealed class Idle : MovementState
    {
        public Idle(Character _character) : base(_character) { }

        public override void Update()
        {
            base.Update();
        }
    }

    public sealed new class Dead : MovementState
    {
        public Dead(Character _character) : base(_character) { }

        public override void Update()
        {
            base.Update();
            character.currentSpeed = Vector3.zero;
        }

        public override void UpdateRotation()
        {
        }
    }

    public sealed class Knockback : MovementState
    {
        public Knockback(Character _character) : base(_character) { }

        public override void Update()
        {
            base.Update();
        }

        public override void UpdateRotation()
        {
        }
    }

    public sealed class PlayerControlledMovement : MovementState
    {
        float timeWithNoRotation = 5f;

        public PlayerControlledMovement(Character _character) : base(_character) { }

        public override void Update()
        {
            base.Update();
            Vector2 dir = ReceiveAnalogInput();
            this.HandleMovement(actor, dir);
        }

        public override void UpdateRotation()
        {
            float mouseX = Input.GetAxis("Mouse X");
            float mouseY = Input.GetAxis("Mouse Y");

            if (Mathf.Abs(mouseX) < 0.1f && Mathf.Abs(mouseY) < 0.1f)
                timeWithNoRotation += Time.deltaTime;
            else
                timeWithNoRotation = 0f;

            if (timeWithNoRotation > 1f)
                base.UpdateRotation();
            else
            { 
                Vector3 cursorPos = InputHandler.GetMouseWorldPoint(LayerMask.NameToLayer("GroundBounds"));
                Vector3 dir = (cursorPos - actor.transform.position).normalized;
                actor.transform.rotation = Quaternion.Lerp(actor.transform.rotation, Quaternion.LookRotation(dir, actor.transform.up), 7.5f * Time.deltaTime);
            }
        }

        public void HandleMovement(GameActor actor, Vector2 dir)
        {
            Player player = actor as Player;

            if (dir.magnitude > 0.1f || actor.currentSpeed.magnitude > 0f)
            {
                Vector3 acceleration = dir.x * Vector3.right
                                       + dir.y * Vector3.forward;

                acceleration = acceleration.normalized * actor.speed;

                actor.currentSpeed = Vector3.Lerp(actor.currentSpeed, new Vector3(acceleration.x, actor.currentSpeed.y, acceleration.z), 1f);
            }
        }

        private static Vector2 ReceiveAnalogInput()
        {
            float x = Input.GetAxis("Horizontal");
            float y = Input.GetAxis("Vertical");
            Vector2 analogInput = new Vector2(x, y);
            return analogInput;
        }
    }

    public sealed class MovingToPosition : MovementState
    {
        private Vector3 targetPos;
        float stopRange;
        UnityAction onEnd;

        public MovingToPosition(Character _character, Vector3 _targetPos, float _stopRange, UnityAction _onEnd)
            : base(_character)
        {
            this.targetPos = _targetPos;
            this.stopRange = _stopRange;
            this.onEnd = _onEnd;
        }

        public override void OnEnter()
        {
            base.OnEnter();
        }

        public override void OnExit()
        {
            base.OnExit();
            character.currentSpeed = Vector3.zero;

            if (onEnd != null)
                onEnd.Invoke();
        }

        public override void Update()
        {
            base.Update();
            float dist = Vector3.Distance(actor.transform.position, targetPos);
            if (dist > stopRange)
            {
                Vector3 dir3D = (targetPos - actor.transform.position).normalized;
                Vector2 dir = new Vector2(dir3D.x, dir3D.z);
                actor.HandleMovement(dir);
            }
            else
                character.SetMovementState(new MovementState.Idle(character));
        }
    }

    public sealed class SeekingTarget : MovementState
    {
        private Transform target;
        float stopRange;
        UnityAction onEnd;

        public SeekingTarget(Character _character, Transform _target, float _stopRange, UnityAction _onEnd = null) 
            : base(_character)
        {
            this.target = _target;
            this.stopRange = _stopRange;
            this.onEnd = _onEnd;
        }

        public override void OnExit()
        {
            base.OnExit();
            actor.currentSpeed = Vector3.zero;
            if (onEnd != null)
                onEnd.Invoke();
        }

        public override void Update()
        {
            base.Update();
            float dist = Vector3.Distance(actor.transform.position, target.transform.position);
            if (dist > stopRange)
            {
                Vector3 dir3D = (target.transform.position - actor.transform.position).normalized;
                Vector2 dir = new Vector2(dir3D.x, dir3D.z);
                actor.HandleMovement(dir);
            }
            else
                actor.currentSpeed = Vector3.zero;
        }    
    }

    public sealed class AvoidingTarget : MovementState
    {
        private Transform target;
        float stopRange;
        UnityAction onEnd;

        public AvoidingTarget(Character _character, Transform _target, float _stopRange, UnityAction _onEnd = null)
            : base(_character)
        {
            this.target = _target;
            this.stopRange = _stopRange;
            this.onEnd = _onEnd;
        }

        public override void OnExit()
        {
            base.OnExit();
            actor.currentSpeed = Vector3.zero;
            if (onEnd != null)
                onEnd.Invoke();
        }

        public override void Update()
        {
            base.Update();
            float dist = Vector3.Distance(actor.transform.position, target.transform.position);
            if (dist > stopRange)
            {
                Vector3 dir3D = (actor.transform.position - target.transform.position).normalized;
                Vector2 dir = new Vector2(dir3D.x, dir3D.z);
                actor.HandleMovement(dir);
            }
            else
                actor.currentSpeed = Vector3.zero;
        }
    }
}
