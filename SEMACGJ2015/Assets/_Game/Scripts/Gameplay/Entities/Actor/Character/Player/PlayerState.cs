using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PlayerState : CharacterState
{
    protected Player player;

    public PlayerState(Player _player)
        : base(_player)
    {
        this.player = _player;
    }

    public override void Update() 
    {
        base.Update();
    }

    public new class Default : CharacterState.Default
    {
        public Default(Player _player) : base(_player) { }

        public override void AllocateCommands()
        {
            commands.Add(typeof(CharacterCommand.Attack), new PlayerCommand.Attack());
        }
    }
        
}