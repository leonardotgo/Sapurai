﻿using UnityEngine;
using System.Collections;

public class ItemChest : Character {

    public GameObject chestLidPivot;
    public Transform spawnItemPosition;

    public Item[] possibleChestItems;

    [HideInInspector]
    public bool opened;

	protected override void Start () {
        base.Start();
        this.opened = false;
        this.unKnockable = true;
	}

    public override void OnDeath()
    {
        if(!opened)
        {
            opened = true;
            StartCoroutine(ChestOpenAnimation());
        }
    }

    public override void Flash(Color color, float duration)
    {
    }

    private IEnumerator ChestOpenAnimation()
    {
        float time = 0f;
        while(time < 0.5f)
        {
            time += Time.deltaTime;
            chestLidPivot.transform.Rotate(Vector3.right, -180f * Time.deltaTime, Space.Self);
            yield return null;
        }

        int itemId = Random.Range(0, possibleChestItems.Length - 1);
        Item item = Instantiate(possibleChestItems[itemId]);
        item.transform.position = spawnItemPosition.position;
        item.transform.rotation = spawnItemPosition.rotation;
    }
}
