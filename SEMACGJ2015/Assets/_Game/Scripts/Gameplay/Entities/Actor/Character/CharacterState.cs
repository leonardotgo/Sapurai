﻿using UnityEngine;
using System.Collections;

public class CharacterState : ActorState {

    protected Character character;

    public CharacterState(Character _character)
        : base(_character)
    {
        this.character = _character;
    }

    public override void OnEnter() { }
    public override void OnExit() { }
    public override void Update()
    {
        base.Update();
    }

    public override void AllocateCommands() { }

    public class Default : CharacterState
    {
        Vector3 gravity = Vector3.down * 9.87f;

        public Default(Character _character) : base(_character) { }

        public override void Update()
        {
            base.Update();

            ApplyGravityPhysics();
        }

        private void ApplyGravityPhysics()
        {
            if (character.isGrounded)
                character.currentSpeed.y = 0f;
            else
                character.currentSpeed += gravity * Time.deltaTime;
        }

        public override void AllocateCommands()
        {
            commands.Add(typeof(CharacterCommand.Attack), new CharacterCommand.Attack());
        }
    }

    public sealed class Dead : CharacterState
    {
        public Dead(Character _character) : base(_character) { }

        public override void OnEnter()
        {
            CharacterCommand.StopMovement stopMoving = new CharacterCommand.StopMovement();
            stopMoving.Execute(character);
            character.StartCoroutine(DeathRoutine());
        }

        private IEnumerator DeathRoutine()
        {
            yield return new WaitForSeconds(1f);
            ObjectFader.FadeAlphaHierarchy(character.gameObject, 0f, 2f);
            yield return new WaitForSeconds(2.1f);
            character.gameObject.SetActive(false);
        }
    }
}
