﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelNumberText : MonoBehaviour {

    public Text UIText;
    public Image panel;

    // Use this for initialization
    private void OnEnable() {
        panel.gameObject.SetActive(true);
        StartCoroutine(PositionAnimationRoutine());
        StartCoroutine(ColorAnimationRoutine());
	}

    private IEnumerator PositionAnimationRoutine()
    {
        float speed = 50f;
        float time = 0f;
        RectTransform t = UIText.GetComponent<RectTransform>();
        Vector3 originalPosition = t.anchoredPosition3D;

        while(time < 4f)
        {
            time += Time.deltaTime;
            t.anchoredPosition3D += Vector3.left * speed * Time.deltaTime;
            yield return null;
        }

        t.anchoredPosition3D = originalPosition;
    }
	
	private IEnumerator ColorAnimationRoutine()
    {
        UIText.SetAlpha(0f);
        panel.SetAlpha(0f);
        UIText.FadeAlpha(1f, 1f);
        panel.FadeAlpha(0.6f, 1f);
        yield return new WaitForSeconds(2f);
        UIText.FadeAlpha(0f, 2f);
        panel.FadeAlpha(0, 2f);
    }

    
}
