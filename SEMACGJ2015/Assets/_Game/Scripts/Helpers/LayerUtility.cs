﻿using UnityEngine;
using System.Collections;

public class LayerUtility : MonoBehaviour {

	public static bool IsInLayerMask(int layer, LayerMask layerMask)
    {
        return (layerMask.value & (int)Mathf.Pow(2, layer)) != 0;
    }
}
